#include <sys/poll.h>
#include <sys/epoll.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/sendfile.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <string>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <iostream>
#include <thread>
#include <vector>
#include <mutex>
#include <condition_variable>
#include <deque>
#include <queue>
#include <list>
#include <set>

static int
make_socket_non_blocking (int sfd)
{
  int flags, s;

  flags = fcntl (sfd, F_GETFL, 0);
  if (flags == -1)
    {
      perror ("fcntl");
      return -1;
    }

  flags |= O_NONBLOCK;
  s = fcntl (sfd, F_SETFL, flags);
  if (s == -1)
    {
      perror ("fcntl");
      return -1;
    }

  return 0;
}

inline bool addOrMod(int epfd, int fd, bool add) {
    /*int rr = make_socket_non_blocking(fd);
    if (rr < 0) {
        std::cerr << "make_socket_non_blocking" << std::endl;
    }*/
            
    epoll_event ev;
    ev.events = EPOLLIN | EPOLLPRI | EPOLLERR | EPOLLHUP | EPOLLRDHUP | EPOLLONESHOT;
    ev.data.fd = fd;
    int op;
    if (add) {
        op = EPOLL_CTL_ADD;
    } else {
        op = EPOLL_CTL_MOD;
    }
    int res = epoll_ctl(epfd, op, fd, &ev);
    if (res < 0) {
        std::cerr << "epoll_ctl1 " << errno << " " << epfd << " " << fd << std::endl;
        return false;
    }
    return true;
}

inline bool addFd(int epfd, int fd) {
    return addOrMod(epfd, fd, true);
}

inline bool modFd(int epfd, int fd) {
    return addOrMod(epfd, fd, false);
}

class BlockedQueue {
private:
    
    const int epfd;
    const int rootFd;
    
public:
    
    BlockedQueue(int rootFd)
        : rootFd(rootFd)
        , epfd(epoll_create(1)) // size игнорируется
    {
        addFd(epfd, rootFd);
    }
    
    void push(int connFd) {
        modFd(epfd, connFd);
    }
    
    void pushNewFd(int connFd) {
        modFd(epfd, rootFd);
        addFd(epfd, connFd);
    }    
    
    int pop() {
        epoll_event events[1];
        const int nfds = epoll_wait(epfd, events, 1, -1);
        if (nfds <= 0) {
            std::cerr << "epoll_wait" << std::endl;
        }
        const int result = events[0].data.fd;
        return result;
    }

};

inline bool endswitch(const std::string &first, const std::string &second) {
    if (first.size() < second.size()) {
        return false;
    }
    int i = first.size() - 1;
    int j = second.size() - 1;
    while (j >= 0) {
        if (first[i] != second[j]) {
            return false;
        }
        i--;
        j--;
    }
    return true;
}

std::string getPageName(const std::string &request) {
    const std::string beginRequest("GET ");
    const std::string endRequest("HTTP");
    const std::string optionsInRequest("?");
    
    size_t beginPos = request.find(beginRequest) + beginRequest.size();
    size_t endResult = request.find(endRequest) - 1;
    size_t optionsResult = request.find(optionsInRequest);
    if (optionsResult != std::string::npos) {
        endResult = optionsResult;
    }
    
    std::string res = request.substr(beginPos, endResult - beginPos);
    if (res[0] == '/') {
        res = res.substr(1);
    }
    return res;
}

void worker(BlockedQueue &queue, const int rootFd, const std::string &dir) {
    while (true) {
        const int connFd = queue.pop();
        if (connFd == rootFd) {
            const int newFd = accept(connFd, NULL, NULL);
            if (newFd < 0) {
                std::cerr << "Cannot accept connection " << errno << std::endl;
                return;
            } else {
                //std::cout << "Connection successful" << std::endl;
            }
            
            queue.pushNewFd(newFd);
            continue;
        }
        
        bool errOrEndSocket = false;
        std::string tester;
        while (true) {
            char test[1024];
            const int countRead = read(connFd, test, 1024);
            if (countRead < 0) {
                std::cerr << std::this_thread::get_id() << " " << "error while read request " << connFd << std::endl;
                errOrEndSocket = true;
                break;
            } else if (countRead == 0) {
                //std::cerr << std::this_thread::get_id() << " " << "Close connection " << connFd << std::endl;
                errOrEndSocket = true;
                break;
            }
            tester += std::string(test, countRead);
            //std::cout << std::string(test) << std::endl;
            if (endswitch(tester, "\r\n\r\n")) {
                break;
            }
        }
        if (errOrEndSocket) {
            close(connFd);
            continue;
        }
        
        const std::string &fileName = getPageName(tester);
        //std::cout << dir + fileName << std::endl;
        
        std::string response;
        int fd = open((dir + fileName).c_str(), O_RDONLY);
        bool notFound = false;
        if (fd == -1) {
            notFound = true;
            response = std::string("HTTP/1.0 404 Not Found\r\n") + 
                std::string("Content-Type: text/html\r\n") + 
                std::string("\r\n");
        } else {
            response = std::string("HTTP/1.0 200 OK\r\n") + 
                std::string("Content-Type: text/html\r\n") + 
                std::string("\r\n");
        }
        int allWrite = 0;
        bool errorWrite = false;
        while (allWrite != response.size()) {
            const int countWrite = write(connFd, response.c_str() + allWrite, response.size() - allWrite);
            if (countWrite < 0) {
                std::cerr << std::this_thread::get_id() << " "  << "error while send response" << std::endl;
                errorWrite = true;
                break;
            }
            allWrite += countWrite;
        }
        if (errorWrite) {
            close(connFd);
            continue;
        }        
        
        if (!notFound) {        
            /* get the size of the file to be sent */
            struct stat stat_buf;
            fstat(fd, &stat_buf);

            /* copy file using sendfile */
            off_t offset = 0;
            int rc = sendfile (connFd, fd, &offset, stat_buf.st_size);
            if (rc == -1) {
                close(connFd);
                continue;
            }
            close(fd);
        } else {
            close(connFd);
            continue;
        }
        
        close(connFd);
    }
}

int main(int argc, char* argv[]) {
    daemon(0, 0);
    
    int c;
    opterr = 0;

    int portNo = 0;
    std::string address;
    std::string directory;

    while ((c = getopt (argc, argv, "h:p:d:")) != -1) {
        switch (c)
        {
        case 'p':
            portNo = std::atoi(optarg);
            break;
        case 'h':
            address = std::string(optarg);
            break;
        case 'd':
            directory = std::string(optarg);
            break;
        }
    }
    
    if((portNo > 65535) || (portNo < 2000)) {
        std::cerr << "Please enter a port number between 2000 - 65535" << std::endl;
        return 0;
    }
    
    if (directory[directory.size() - 1] != '/') {
        directory += "/";
    }
    
    const int countThreads = std::thread::hardware_concurrency();
    std::cout << countThreads << std::endl;
    if (countThreads <= 0) {
        std::cerr << "countThreads <= 0" << std::endl;
        return 0;
    }
    
    //create socket
    int listenFd = socket(AF_INET, SOCK_STREAM, 0);
    
    if(listenFd < 0) {
	std::cerr << "Cannot open socket" << std::endl;
	return 0;
    }
    
    sockaddr_in svrAdd;    
    svrAdd.sin_family = AF_INET;
    inet_aton(address.c_str(), &(svrAdd.sin_addr));
    svrAdd.sin_port = htons(portNo);
    
    //bind socket
    if(bind(listenFd, (sockaddr*)&svrAdd, sizeof(svrAdd)) < 0) {
        std::cerr << "Cannot bind" << std::endl;
        return 0;
    }
    
    listen(listenFd, 64);
    
    BlockedQueue queue(listenFd);
    
    std::vector<std::thread> threads;
    for (int i = 0; i < countThreads - 1; i++) {
        threads.emplace_back(worker, std::ref(queue), listenFd, directory);
        threads[threads.size() - 1].detach();
    }    
    
    worker(queue, listenFd, directory);
}
